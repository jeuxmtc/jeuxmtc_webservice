package controllers;
import play.*;
import play.api.db.*;
import play.libs.Json;
import play.mvc.*;
import play.data.*;
import static play.data.Form.*;
import java.util.* ;

import views.html.*;
import factory.QuestionFactory;
import factory.UserFactory;
import models.*;

public class Application extends Controller {
  
    public static Result index() {


    	String user = UserFactory.register("toto", "toto@toto.fr", "passwordDeOuf");
    	if(user=="ok")
    		System.out.println("L'utilisateur a bien été ajouté !");
    	else 
    		System.out.println(user);
    		
    	User u1 = UserFactory.login("toto", "passwordDeOuf");
    	User u2 = UserFactory.login("toto", "passwordDeOuf2");
    	if (u1!=null)
    		System.out.println("Oh yeah ! " + u1.username + " trouvé !");
    	if(u2 == null)
    		System.out.println("Oh yeah ! u2 pas trouvé !");
    	return ok("Olé");
    }
    
    public static Result questions(String langue,Integer nb){	
		if(nb>40){
		    nb=40;
		}	    
		ArrayList<Question> questions =  QuestionFactory.getQuestions(langue, nb);
		return ok(Json.toJson(questions));
    }

   public static Result create_user(String username, String email, String password){
		Map<String, String> message = new HashMap<String, String>();
   		if(username==null||email==null||password==null) {
   			message.put("success", "false");
   			message.put("erreur", "merci de fournir tous les champs !");   			  		
   			return ok(Json.toJson(message));
   		}

		String user = UserFactory.register(username, email, password);
		if(user == "ok") {
			message.put("success", "true");
			return ok(Json.toJson(message));			
		} else {
			message.put("success", "false");
			message.put("erreur", user);
			return ok(Json.toJson(message));
		}
    }

    public static Result login_user(String username, String password){
  		Map<String, String> message = new HashMap<String, String>();
     		if(username==null||password==null) {
     			message.put("success", "false");
     			message.put("erreur", "merci de fournir tous les champs !");   			  		
     			return ok(Json.toJson(message));
     		}

  		User user = UserFactory.login(username, password);
  		if(user != null) {
  			return ok(Json.toJson(user));
  		} else {
  			message.put("success", "false");
  			message.put("erreur", "impossible de trouver l'utilisateur");
  			return ok(Json.toJson(message));
  		}
    }

    public static Result login() {
      return ok(
          views.html.app.login.render(form(Login.class))
      );
    }

   public static Result authenticate() {
      Form<Login> userForm = form(Login.class).bindFromRequest();
      if (userForm.hasErrors()) {
          return badRequest(views.html.app.login.render(userForm));
      } else {
          session().clear();
          session("username", userForm.get().username);
          return redirect(
              routes.Natures.index()
          );
      }
  }

  public static Result logout() {
      session().clear();
      flash("success", "Vous vous êtes bien déconnecté");
      return redirect(
          routes.Application.login()
      );
  }
}

















