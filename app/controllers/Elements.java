package controllers;

import play.*;
import play.api.db.*;
import play.mvc.*;
import play.data.*;
import static play.data.Form.*;
import java.util.* ;
import models.Nature;
import models.Element;
import models.ElementBase;
import views.html.app.element.*;

@Security.Authenticated(Secured.class)
public class Elements extends Controller {

	final static Form<Element> elementForm = form(Element.class);

	public static Result index() {
		List<Element> q =  Element.find.all();
		return ok(index.render(q));
	}

	public static Result create() {
		Element existingElement = new Element();
		List<models.Nature> nature = models.Nature.find.all();
		List<models.ElementBase> eb = models.ElementBase.find.all();
		return ok(create.render(eb,nature,elementForm.fill(existingElement)));
	}

	public static Result edit(Integer id) {
		Element existingElement = Element.find.byId(id);
		List<models.Nature> nature = models.Nature.find.all();
		List<models.ElementBase> eb = models.ElementBase.find.all();
        return ok(edit.render(eb,nature,elementForm.fill(existingElement)));
	}

	public static Result delete(Integer id) {
		Element existingElement = Element.find.byId(id);
		existingElement.delete();
		List<Element> q =  Element.find.all();
		return ok(index.render(q));
	}

	public static Result showElement(int id) {
	  models.Element element = models.Element.find.where().eq("ID", id).findUnique();

	  return ok(show.render(element));
	  
	}

	public static Result update(Integer id) {
		Form<Element> filledForm = elementForm.bindFromRequest();
        List<models.Nature> nature = models.Nature.find.all();
        List<models.ElementBase> eb = models.ElementBase.find.all();
        if(filledForm.hasErrors()) {
            return badRequest(edit.render(eb,nature,filledForm));
        } else {
            Element updated = filledForm.get();
            updated.ID = id;
            updated.update();
            return ok(edit.render(eb,nature,elementForm.fill(updated)));
        }
	}

	public static Result insert() {
		Form<Element> filledForm = elementForm.bindFromRequest();
        List<models.Nature> nature = models.Nature.find.all();
        List<models.ElementBase> eb = models.ElementBase.find.all();
        if(filledForm.hasErrors()) {
            return badRequest(edit.render(eb,nature,filledForm));
        } else {
            Element created = filledForm.get();
            created.save();
            return ok(edit.render(eb,nature,elementForm.fill(created)));
        }
	}
}