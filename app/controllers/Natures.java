package controllers;

import play.*;
import play.api.db.*;
import play.mvc.*;
import play.data.*;
import static play.data.Form.*;
import java.util.* ;
import models.Nature;

import views.html.app.nature.*;

@Security.Authenticated(Secured.class)
public class Natures extends Controller {

	final static Form<Nature> natureForm = form(Nature.class);

	public static Result index() {
		List<Nature> q =  Nature.find.all();
		return ok(index.render(q));
	}

	public static Result create() {
		Nature existingNature = new Nature();
		return ok(create.render(natureForm.fill(existingNature)));
	}

	public static Result edit(Integer id) {
		Nature existingNature = Nature.find.byId(id);
        return ok(edit.render(natureForm.fill(existingNature)));
	}

	public static Result delete(Integer id) {
		Nature existingNature = Nature.find.byId(id);
		existingNature.delete();
		List<Nature> q =  Nature.find.all();
		return redirect("/nature");
	}

	public static Result update(Integer id) {
		Form<Nature> filledForm = natureForm.bindFromRequest();
        
        if(filledForm.hasErrors()) {
            return badRequest(edit.render(filledForm));
        } else {
            Nature updated = filledForm.get();
            updated.ID = id;
            updated.update();
            flash("success", "Nature mise à jours");
            return ok(edit.render(natureForm.fill(updated)));
        }
	}

	public static Result insert() {
		Form<Nature> filledForm = natureForm.bindFromRequest();
        if(filledForm.hasErrors()) {
            return ok(create.render(filledForm));
        } else {
            Nature created = filledForm.get();
            created.save();
            flash("success", "Nature ajoutée");
            return redirect("/nature");
        }
    }
}