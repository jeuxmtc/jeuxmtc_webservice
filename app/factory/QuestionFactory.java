package factory;
import play.*;
import play.api.db.*;
import play.mvc.*;

import java.util.* ;

import views.html.*;
import models.*;
import java.util.ArrayList ;
import com.avaje.ebean.*;

public class QuestionFactory {
	
	public static ArrayList<Question> getQuestions(String lang, int nbQuestions){
		ArrayList<Question> questions = new ArrayList<Question>();

		for (int i = 0; i < nbQuestions; i++) {
			int type = (int)(Math.random() * 4) + 1;
			switch (type) {
			case 1:
				questions.add(getQuestionType1(lang));
				break;
			case 2:
			case 3:
				questions.add(getQuestionType23(lang, type));
				break;
			case 4:
				questions.add(getQuestionType4(lang));
				break;
			default:
				System.out.println("Unknown value " + Integer.toString(type));
			}
		}

		return questions;
	}
    
   private static Question getQuestionType1(String lang) {
    	Question question = new Question(1) ;
    	int nbElementsAPlacer = (int)(Math.random() * (5-1)) + 1;
    	
    	// Récupération des éléments de base

		//List<Element> elements = Element.find.where("nature1_ID", "base");
		Nature natureBase = Nature.find.where().eq("libelle", "Elément").findUnique();
		List<Element> elements = Element.find.where().eq("nature1_ID", natureBase.ID).findList();
		List<ElementBase> elementsBase = ElementBase.find.all();

		for (ElementBase elementBase : elementsBase) {
			for (Element element : elements) {
				if (element.parent_ID == elementBase.ID) {
					question.addFixedElement(element);
					break;
				}
			}
		}
		
		// On enlève le nature 'base'
    	int nbNatures = Nature.find.findRowCount() - 1;
		int randomIdNature = natureBase.ID;

		// Recherche d'une autre nature que celle de base 
		while (randomIdNature == natureBase.ID) {
			randomIdNature = (int)(Math.random() * (nbNatures - 1) + 1);
			//int randomIdNature = (int)(Math.random() * (nbNatures-1)) + 1;
		}
    	Nature nature = Nature.find.byId(randomIdNature);
    	// Elements base sélectionnés
    	List<Element> elementsQuestion = Element.find.where().eq("nature1_ID", nature.ID).findList();
		if (elementsQuestion.size() == 0)
			elementsQuestion = Element.find.where().eq("nature2_ID", nature.ID).findList();

		String explaination = getMessage(lang, "explaination1");
		explaination += " " + nature.getLibelle() + " " + getMessage(lang, "explaination12");

		for (Element e : elementsQuestion) {
			for (Element fe: question.getFixedElements()) {
				if (e.parent_ID == fe.parent_ID) {
					explaination += "\n" + e.getLibelle() + " -> " + fe.getLibelle();
					break;
				}
			}
			
		}

    	for(int i = 0 ; i < nbElementsAPlacer ; i++){
			if (elementsQuestion.size() == 0) break;
			int randomElementId = (int)(Math.random() * (elementsQuestion.size() - 1));

			int position = -1;
			for (int j = 0; j < question.getFixedElements().size(); j++) {
				Element e = question.getFixedElements().get(j);
				if (e.parent_ID == elementsQuestion.get(randomElementId).parent_ID) {
					position = j;
					break;
				}
			}

			question.addMovableElement(position, elementsQuestion.get(randomElementId));
			elementsQuestion.remove(randomElementId);
    	}


    	question.setIntitule(getMessage(lang, "question1") + " " + nature.libelle);

		// Création de l'explication
    	// ex : ELEMENT se place sous ELEMENT

    	question.setExplaination(explaination);
    	
    	//question.setExplaination(explaination);
    	return question;
    }

	private static Question getQuestionType23(String lang, int type) {
    	Question question = new Question(type) ;
    	int nbElementsAPlacer = (int)(Math.random() * (5-1)) + 1;

    	// Récupération des éléments de base
		Nature natureBase = null;
		if (type == 2) {
			natureBase = Nature.find.where().eq("libelle", "Elément").findUnique();
			//List<Element> elements = Element.find.where().eq("nature1_ID", natureBase.ID).findList();
		} else {
			List<Nature> natures = Nature.find.all();
			int natureid = (int)(Math.random() * (natures.size() - 1));
			natureBase = natures.get(natureid);
		}

		List<Element> elements = Element.find.where().eq("nature1_ID", natureBase.ID).findList();
		if (elements.size() == 0)
			elements = Element.find.where().eq("nature2_ID", natureBase.ID).findList();
		List<ElementBase> elementsBase = ElementBase.find.all();

		int firstElement = (int)(Math.random() * (elementsBase.size() - 1));
		for (int i = 0; i < elementsBase.size(); i++) {
			int index = (i + firstElement) % elementsBase.size();
			for (Element element : elements) {
				if (element.parent_ID == elementsBase.get(index).ID) {
					question.addFixedElement(element);
					break;
				}
			}
		}
		
		// On enlève le nature 'base'
    	int nbNatures = Nature.find.findRowCount() - 1;
		String explaination = "";

    	for(int i = 0 ; i < nbElementsAPlacer ; i++){
			// Recherche d'une autre nature que celle de base 

			int randomIdNature = natureBase.ID;
			while (randomIdNature == natureBase.ID) {
				randomIdNature = (int)(Math.random() * (nbNatures - 1) + 1);
			}
			Nature nature = Nature.find.byId(randomIdNature);

			// Elements base sélectionnés
			List<Element> elementsQuestion = Element.find.where().eq("nature1_ID", nature.ID).findList();
			if (elementsQuestion.size() == 0)
				elementsQuestion = Element.find.where().eq("nature2_ID", nature.ID).findList();

			if (explaination != "") explaination += "\n";
			explaination += getMessage(lang, "explaination1");
			explaination += " " + nature.getLibelle() + " " + getMessage(lang, "explaination12");

			for (Element e : elementsQuestion) {
				for (Element fe: question.getFixedElements()) {
					if (e.parent_ID == fe.parent_ID) {
						explaination += "\n" + e.getLibelle() + " -> " + fe.getLibelle();
						break;
					}
				}
			
			}

			int randomElementId = (int)(Math.random() * (elementsQuestion.size() - 1));

			int position = -1;
			for (int j = 0; j < question.getFixedElements().size(); j++) {
				Element e = question.getFixedElements().get(j);
				if (e.parent_ID == elementsQuestion.get(randomElementId).parent_ID) {
					position = j;
					break;
				}
			}


			question.addMovableElement(position, elementsQuestion.get(randomElementId));
			//elementsQuestion.clear();
    	}
    		
		

    	question.setIntitule(getMessage(lang,"question2"));
    	
    	// Création de l'explication
    	// ex : Par exemple, l'élément FEU est parent de l'élément Pouce. Le pouce doit donc
    	// être placé dans l'élément FEU.
    	//String explaination = rows.get(XXX).getString("message") ;
		question.setExplaination(explaination);

    	return question;
    }

	private static Question getQuestionType4(String lang) {
		Question question = new Question(4);

		// relation == 0 ? engendrement : domination
		int relation = (int)Math.random();

		List<ElementBase> elementsBase = ElementBase.find.all();
		ElementBase eb1 = elementsBase.get((int)(Math.random() * (elementsBase.size() - 1)));
		ElementBase eb2 = null;
		ElementBase eb3 = elementsBase.get((int)(Math.random() * (elementsBase.size() - 1)));
		ElementBase eb4 = null;

		System.out.println("Relation " + ((relation == 0) ? "Engendrement" : "Dominiation"));

		int index1 = (relation == 1) ? eb1.domin_ID : eb1.engend_ID;
		int index2 = (relation == 1) ? eb3.domin_ID : eb3.engend_ID;
		
		for (ElementBase eb : elementsBase) {
			if (eb.ID == index1)
				eb2 = eb;
			if (eb.ID == index2)
				eb4 = eb;
		}

		List<Nature> natures = Nature.find.all();
		int n1 = (int)(Math.random() * (natures.size() - 1));
		int n2 = (int)(Math.random() * (natures.size() - 1));
		int n3 = (int)(Math.random() * (natures.size() - 1));
		int n4 = (int)(Math.random() * (natures.size() - 1));

		System.out.println("Nature 1 - " + natures.get(n1).getLibelle());
		System.out.println("Nature 2 - " + natures.get(n2).getLibelle());
		System.out.println("Nature 3 - " + natures.get(n3).getLibelle());
		System.out.println("Nature 4 - " + natures.get(n4).getLibelle());

		Element e1 = Element.find
			.where()
			.eq("nature1_ID", natures.get(n1).ID)
			.eq("parent_ID", eb1.ID).findUnique();
		if (e1 == null)
			e1 = Element.find
				.where()
				.eq("nature2_ID", natures.get(n1).ID)
				.eq("parent_ID", eb1.ID).findUnique();
		Element e2 = Element.find
			.where()
			.eq("nature1_ID", natures.get(n2).ID)
			.eq("parent_ID", eb2.ID).findUnique();
		if (e2 == null)
			e2 = Element.find
				.where()
				.eq("nature2_ID", natures.get(n2).ID)
				.eq("parent_ID", eb2.ID).findUnique();
		Element e3 = Element.find
			.where()
			.eq("nature1_ID", natures.get(n3).ID)
			.eq("parent_ID", eb3.ID).findUnique();
		if (e3 == null)
			e3 = Element.find
				.where()
				.eq("nature2_ID", natures.get(n3).ID)
				.eq("parent_ID", eb3.ID).findUnique();
		Element e4 = Element.find
			.where()
			.eq("nature1_ID", natures.get(n4).ID)
			.eq("parent_ID", eb4.ID).findUnique();
		if (e4 == null)
			e4 = Element.find
				.where()
				.eq("nature2_ID", natures.get(n4).ID)
				.eq("parent_ID", eb4.ID).findUnique();

		List<Element> elements = Element.find.where().eq("nature1_ID", natures.get(n4).ID).findList();
		if (elements.size() == 0)
			elements = Element.find.where().eq("nature2_ID", natures.get(n4).ID).findList();

		
		String q4a = getMessage(lang,"question4a");

		String q4b = getMessage(lang,"question4b");
		

		question.setIntitule(e1.getLibelle() + " " + q4a + " " + e2.getLibelle() + " " + q4b + " " + e3.getLibelle() + " " + q4a);
		
		// Création de l'explication
    	// ex : Par exemple, l'élément FEU est parent de l'élément Pouce. Le pouce doit donc
    	// être placé dans l'élément FEU.
    	//String explaination = rows.get(XXX).getString("message") ;
    	
		// TODO: Vérifier l'ordre des éléments
		int bonneRep = -1;
		for (int i = 0; i < elements.size(); i++) {
			Element e = elements.get(i);
			if (e.ID == e4.ID)
				bonneRep = i;
			question.addFixedElement(e);
		}

		question.addMovableElement(bonneRep, e4);

		return question;
	}
	
	private static String getMessage(String lang, String code ){
		String sql = "select distinct message from questionlibelle where langue=:lang and code='" + code + "'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
		sqlQuery.setParameter("lang", lang);

		List<SqlRow>rows = sqlQuery.findList();
		String message = rows.get(0).getString("message");
		
		return message ; 
	}
	
	
}


















