package models;
import play.db.ebean.Model;

import javax.persistence.*;
import play.data.validation.*;
import play.libs.*;

@Entity
public class User extends Model {
	@Id
	public int ID;
	@Constraints.Required(message="Le nom d'utilisateur est requis")
	public String username;
	@Constraints.Required(message="L'email est requis")
	public String email;
	@Constraints.Required(message="Le mot de passe est requis")
	public String password;

	public User(String username,String email)  {
		this.username = username;
		this.email = email;
	}

	public User(String username,String email, String password)  {
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public static String encodePassword(String password){
		Crypto c = new Crypto();
     	return c.sign(password);
     }

    // Getters et setters
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getID() {
		return ID;
	}

	public static Finder<Integer,User> find = new Finder<Integer,User>(
		Integer.class, User.class
		); 

}