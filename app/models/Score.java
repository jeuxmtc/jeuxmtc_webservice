package models;
import play.db.ebean.Model;

import javax.persistence.*;
import play.data.validation.*;

@Entity
public class Score extends Model {
	@Id
	public int ID;

	@Constraints.Required(message="Le score est requis")
	public int score;
	@Constraints.Required(message="L'utilisateur est requis")
	public int user_ID;

    // Getters et setters
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getUser_ID() {
		return user_ID;
	}

	public void setUser_ID(int user_ID) {
		this.user_ID = user_ID;
	}

	public int getID() {
		return ID;
	}
	
	public static Finder<Integer,Score> find = new Finder<Integer,Score>(
		Integer.class, Score.class
		); 

}