package models;
import play.db.ebean.Model;

import javax.persistence.*;
import play.data.validation.*;

@Entity
public class Nature extends Model {
    @Id
    public int ID;

    @Constraints.Required(message="Difficulté est requis")
    public int difficulte ;
    
    @Constraints.Required(message="Libelle est requis")
    public String libelle ;

    public String aide ;
    public String reference ;
    
    // Getters et setters
	public String getLibelle() {
		return libelle;
	}
	public static Nature byId(int id){
		return Nature.find.byId(id);
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getAide() {
		return aide;
	}

	public void setAide(String aide) {
		this.aide = aide;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public int getID() {
		return ID;
	}
	
	public int getDifficulte() {
		return difficulte;
	}
	public void setDifficulte(int difficulte) {
		this.difficulte = difficulte;
	}
    
     public static Finder<Integer,Nature> find = new Finder<Integer,Nature>(
	    Integer.class, Nature.class
	  );

     
    
}