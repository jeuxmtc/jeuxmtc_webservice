# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table element (
  id                        integer auto_increment not null,
  parent_id                 integer,
  nature1_id                integer,
  nature2_id                integer,
  url_image                 varchar(255),
  libelle                   varchar(255),
  aide                      varchar(255),
  reference                 varchar(255),
  constraint pk_element primary key (id))
;

create table element_base (
  id                        integer auto_increment not null,
  engend_id                 integer,
  domin_id                  integer,
  nature1_id                integer,
  nature2_id                integer,
  url_image                 varchar(255),
  libelle                   varchar(255),
  aide                      varchar(255),
  reference                 varchar(255),
  constraint pk_element_base primary key (id))
;

create table nature (
  id                        integer auto_increment not null,
  difficulte                integer,
  libelle                   varchar(255),
  aide                      varchar(255),
  reference                 varchar(255),
  constraint pk_nature primary key (id))
;

create table score (
  id                        integer auto_increment not null,
  score                     integer,
  user_id                   integer,
  constraint pk_score primary key (id))
;

create table user (
  id                        integer auto_increment not null,
  username                  varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table element;

drop table element_base;

drop table nature;

drop table score;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

